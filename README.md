# unity-teoria_gier_praca_licencjacka

Projekt umożliwa matematyczną analizę problemów decyzyjnych, które powstały pomiędzy dwoma uczestnikami danej sytuacji (gry).
Analiza uwzględna gry o sumie zero (czyli takich, gdzie wygrana jednego gracza (uczestnika) wiąże się z taką samą przegraną u gracza drugiego)

## Wykorzystane technologie
- [Silnik graficzny Unity](https://unity3d.com/)

## Funkcjonalność aplikacji
- Rozwiązywanie gier ze strategiami czystymi
- Usuwanie strategii zdominowanych
- Rozwiązywanie gier bez strategii czystych
  - Metoda dla małych gier (2x2)
  - Metoda graficzna dla gier 2xm lub nx2
  - Metoda dla dużych gier (conajmniej 3x3)
  
## Możliwości użytkonika:
- Tworzenie własnych gier (od 2 do 5 strategii dla gracza)
- Własne nazwy gracza
- Analizowanie przygotowanych już wcześniej gier (zapisanych za pomocą ScriptableObjects lub XML*)

* Domyślnie gry były zapisane jako XML, jednak po zapisie na płycie CD okazało się, że build w Unity sobie z tym nie radzi i zmieniłem na Scriptable Objects

## Odnośniki do pracy
- [Prezentacja](https://github.com/pawelbabiuch/unity-teoria_gier_praca_licencjacka/blob/master/Prezentacja/Pawe%C5%82%20Babiuch%20-%20Prezentacja%20(Teoria%20gier).pdf)
- [Praca licencjacka](https://github.com/pawelbabiuch/unity-teoria_gier_praca_licencjacka/blob/master/Praca/Pawe%C5%82%20Babiuch%20%E2%80%93%20algorytmy%20wyznaczania%20punkt%C3%B3w%20r%C3%B3wnowagi%20w%20grach%20o%20sumie%20zero.pdf)


## Wideo do prezentacji

<a align='center' href="http://www.youtube.com/watch?feature=player_embedded&v=x44RoC62sYc
" target="_blank"><img src="http://img.youtube.com/vi/x44RoC62sYc/0.jpg" 
alt="wideo prezentujace" border="10" /></a>
